package baladeiros.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

// Banco de dados
public class BD {
    public static List<Festa> loadFesta () {
         
        List<Festa> f;
        
        try {
            InputStream fos = new FileInputStream("festas.bld");
            ObjectInputStream ois = new ObjectInputStream(fos);
            f = (List<Festa>) ois.readObject();
            fos.close();
            ois.close();
            
        } catch (Exception e) {
            // e.printStackTrace();
            return new ArrayList<Festa>();
        }
        
        return f;
    }
    
    public static boolean saveFestas(List<Festa> f) {
        
        try {
            FileOutputStream fos = new FileOutputStream("festas.bld", false);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f);
            oos.flush();
            oos.close();
        } catch(Exception e) {
            // e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    public static List<Cliente> loadCliente () {
         
        List<Cliente> c;
        
        try {
            InputStream fos = new FileInputStream("clientes.bld");
            ObjectInputStream ois = new ObjectInputStream(fos);
            c = (List<Cliente>) ois.readObject();
            fos.close();
            ois.close();
            
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<Cliente>();
        }
        
        return c;
    }
    
    public static boolean saveCliente(List<Cliente> f) {
        
        try {
            FileOutputStream fos = new FileOutputStream("clientes.bld", false);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f);
            oos.flush();
            oos.close();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
}
