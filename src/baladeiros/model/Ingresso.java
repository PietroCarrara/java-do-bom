package baladeiros.model;

import java.io.Serializable;

public class Ingresso implements Serializable {
    
    private Festa f;
    private Cliente c;
    
    private int cod;
    
    public Ingresso(Cliente c, Festa f) {
        this.c = c;
        this.f = f;
        
        this.cod = f.getMaxIngressos() - f.getNumIngressos();
        
        c.addIngresso(this);
    }
    
    public int getCod() {
        return cod;
    }
}
