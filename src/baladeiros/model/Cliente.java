package baladeiros.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Cliente implements Serializable {

    private String nome;
    private double dinheiro;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getDinheiro() {
        return dinheiro;
    }

    public void setDinheiro(double dinheiro) {
        this.dinheiro = dinheiro;
    }

    public int getPontinhos() {
        return pontinhos;
    }

    public void setPontinhos(int pontinhos) {
        this.pontinhos = pontinhos;
    }

    public ArrayList<Ingresso> getIngressos() {
        return ingressos;
    }

    public void setIngressos(ArrayList<Ingresso> ingressos) {
        this.ingressos = ingressos;
    }
    private int pontinhos;
    
    private ArrayList<Ingresso> ingressos = new ArrayList<>();
        
    public Cliente(String nome, double dinheiro) {
        this.nome = nome;
        this.dinheiro = dinheiro;
        // Começa sem créditos especiais
        pontinhos = 0;
    }
    
    public void addIngresso(Ingresso i) {
        this.ingressos.add(i);
    }
    
    public void decreaseMoney(double m) {
        this.dinheiro -= m;
    }
}
