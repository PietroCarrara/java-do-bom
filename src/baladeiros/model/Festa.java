package baladeiros.model;

import java.io.Serializable;

/**
 *
 * @author developer
 */
public class Festa implements Serializable {
    
    private String tipoFesta, nome;

    public int maxIngressos, numIngressos;
    
    public double preco;

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
 
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getMaxIngressos() {
        return maxIngressos;
    }

    public void setMaxIngressos(int maxIngressos) {
        this.maxIngressos = maxIngressos;
    }

    public int getNumIngressos() {
        return numIngressos;
    }

    public void setNumIngressos(int numIngressos) {
        this.numIngressos = numIngressos;
    }
    
    public String getTipoFesta() {
        return tipoFesta;
    }

    public void setTipoFesta(String tipoFesta) {
        this.tipoFesta = tipoFesta;
    }
    
    public Ingresso comprar(Cliente c) {
        
        if (numIngressos > 0 && c.getDinheiro() >= preco) {
            Ingresso i = new Ingresso(c, this);
            numIngressos--;
            c.decreaseMoney(this.preco);
            return i;
        }
        
        return null;
    }
}
